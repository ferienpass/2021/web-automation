"""
Verify that the Facebook website works correctly.
"""
from selenium import webdriver

browser = webdriver.Firefox()
browser.get("http://www.facebook.com")
browser.implicitly_wait(5)  # seconds

accept_button = browser.find_element_by_css_selector("button[data-cookiebanner=accept_button]")
accept_button.click()

password = browser.find_element_by_id("pass")
username = browser.find_element_by_id("email")
submit = browser.find_element_by_css_selector("button[name=login]")

username.send_keys("YOUR EMAILID")
password.send_keys("YOUR PASSWORD")
submit.click()

browser.save_screenshot("screenshot-facebook.png")
assert "Facebook" in browser.title

browser.close()
browser.quit()
