Website Automation
==================

Install and use the Python modules we need:

```console
pipenv install
pipenv shell
```

Run your automation code

```console
python facebook.py
```
